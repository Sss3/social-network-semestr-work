package ru.kpfu.form;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
public class PassForm {
    private String old;
    private String newP;
    private String conf;

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    public String getNewP() {
        return newP;
    }

    public void setNewP(String newP) {
        this.newP = newP;
    }

    public String getConf() {
        return conf;
    }

    public void setConf(String conf) {
        this.conf = conf;
    }
}
