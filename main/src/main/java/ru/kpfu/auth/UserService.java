package ru.kpfu.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.kpfu.api.domain.User;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
public class UserService implements UserDetailsService{

    @Autowired
    ru.kpfu.api.service.UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.getByEmail(s);
        if(user == null) throw new UsernameNotFoundException("User with email " + s + " not found");
        return new AuthUser(user);
    }

}
