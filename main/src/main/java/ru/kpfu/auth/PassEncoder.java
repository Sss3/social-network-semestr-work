package ru.kpfu.auth;

import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
public class PassEncoder implements PasswordEncoder{
    @Override
    public String encode(CharSequence charSequence) {
        return this.hash(charSequence.toString());
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return this.hash(charSequence.toString()).equals(s);
    }

    private String hash(String charSequence) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] array = digest.digest(charSequence.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return charSequence;
    }
}
