package ru.kpfu.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.api.domain.Photo;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.service.PhotoService;
import ru.kpfu.api.service.UserService;
import ru.kpfu.util.AuthUtil;

import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import java.io.*;
import java.util.List;
import java.util.Random;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
@Controller
public class PhotoAlbumController {

    @Autowired
    UserService userService;

    @Autowired
    PhotoService photoService;

    @RequestMapping(value = "/album{id}", method = RequestMethod.GET)
    public String albumPage(@PathVariable Long id, ModelMap modelMap) {
        if(id == null || id.equals(0L)) {
            if(AuthUtil.isAuthenticated()) {
                return "redirect:/album" + AuthUtil.getCurrentUser().getId();
            } else {
                return "redirect:/login";
            }
        }
        if(AuthUtil.isAuthenticated() && id.equals(AuthUtil.getCurrentUser().getId())) {
            modelMap.put("edit", true);
        }
        User user = userService.getById(id);
        if(user == null) {
            modelMap.put("message", "User not found");
            return "errors/template";
        }

        modelMap.put("photos", user.getPhotos());

        return "photos";
    }

    @RequestMapping(value = "/upload/photo", method = RequestMethod.POST)
    public @ResponseBody String uploadPhoto(@RequestParam MultipartFile file) {
        if(file != null && !file.isEmpty()) {
            System.out.println("UPLOAD PHOTO");
            BufferedOutputStream stream = null;
            try {
                byte[] bytes = file.getBytes();
                User u = AuthUtil.getCurrentUser();
                File serverFile = save(u.getId(), file.getContentType());
                stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
                Photo photo = new Photo(Const.urlPhoto + Const.separator + u.getId() + Const.separator + serverFile.getName());
                photo = photoService.save(photo);
                List<Photo> photos = u.getPhotos();
                System.out.println(photos.size());
                photos.add(photo);
                userService.save(u);
                return new ObjectMapper().writeValueAsString(photo);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "bad";
    }

    private String generateFileName() {
            char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
            StringBuilder stringBuilder = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < 7; i++){
                char c = chars[random.nextInt(chars.length)];
                stringBuilder.append(c);
            }
            return stringBuilder.toString();
    }

    private File save(Long id, String contentType) {
        File dir = new File(Const.uploadPhoto + File.separator + id);
        if(!dir.exists()) {
            dir.mkdir();
        }
        File f = new File(Const.uploadPhoto + File.separator + id + File.separator + generateFileName() + "." + getContentType(contentType));
        while (f.exists()) {
            f = new File(Const.uploadPhoto + File.separator + id + File.separator + generateFileName() + "." + getContentType(contentType));
        }
        return f;
    }

    private String getContentType(String contentType) {
        try {
            return new ContentType(contentType).getSubType();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

}
