package ru.kpfu.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.domain.WallPost;
import ru.kpfu.api.service.WallPostService;
import ru.kpfu.auth.AuthUser;
import ru.kpfu.json.Post;
import ru.kpfu.util.AuthUtil;

import java.io.IOException;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Controller
public class WallController {

    @Autowired
    private WallPostService wallPostService;


    /*
     * @param to пользователь на чью стену добавляем сообщение
     * @param from пользователь кто постит
     * @return new post (json format)
     */
    @RequestMapping(value = "/sendPost", method = RequestMethod.POST)
    public @ResponseBody String sendPost(@RequestParam Long to, @RequestParam String text) throws JsonProcessingException {
        WallPost wallPost = wallPostService.save(new WallPost(AuthUtil.getCurrentUser(), new User(to), text));
        Post post = new Post(wallPost);
        post.init();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(post);
    }

}
