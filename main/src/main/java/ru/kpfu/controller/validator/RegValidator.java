package ru.kpfu.controller.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.api.service.UserService;
import ru.kpfu.form.RegForm;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Component
public class RegValidator implements Validator {

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return RegForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "reg.fieldIsEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "reg.fieldIsEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "family", "reg.fieldIsEmpty");

        if(!errors.hasFieldErrors("email")){
            RegForm form = (RegForm) o;
            if(!form.getEmail().contains("@")) {
                errors.rejectValue("email", "reg.emailInvalid");
            } else {
                if(userService.getByEmail(form.getEmail()) != null) {
                    errors.rejectValue("email", "reg.emailIsBusy");
                }
            }
        }
    }
}
