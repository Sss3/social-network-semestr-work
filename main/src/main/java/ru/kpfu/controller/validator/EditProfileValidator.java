package ru.kpfu.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.kpfu.form.EditProfileForm;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Component
public class EditProfileValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return EditProfileForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EditProfileForm form = (EditProfileForm) o;
        if(form.getEmail() != null && !form.getEmail().isEmpty() && !form.getEmail().contains("@")) {
            errors.rejectValue("email", "reg.emailInvalid");
        }
    }
}
