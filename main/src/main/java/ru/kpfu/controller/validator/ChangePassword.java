package ru.kpfu.controller.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.api.service.UserService;
import ru.kpfu.form.PassForm;

/**
 * @author Alexeev Vladimir  03.04.2015
 */
@Component
public class ChangePassword implements Validator{

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return PassForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "old", "pass.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newP", "pass.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "conf", "pass.empty");
        PassForm form = (PassForm) o;
        if(!form.getNewP().equals(form.getConf())) {
            errors.rejectValue("newP", "pass.noconf");
            errors.rejectValue("conf", "pass.noconf");
        }
        if(form.getNewP().length() < 7) {
            errors.rejectValue("newP", "pass.length");
        }
    }
}
