package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.api.domain.Friend;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.service.FriendService;
import ru.kpfu.api.service.UserService;
import ru.kpfu.util.AuthUtil;

import java.util.List;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
@Controller
public class FriendController {

    @Autowired
    private FriendService service;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/friends{id}", method = RequestMethod.GET)
    public String rendPage(@PathVariable Long id, ModelMap map) {
        if(id == null || id.equals(0L)) {
            if(AuthUtil.isAuthenticated()) {
                return "redirect:/friends" + AuthUtil.getCurrentUser().getId();
            } else {
                return "redirect:/login";
            }
        }

        if(AuthUtil.isAuthenticated() && id.equals(AuthUtil.getCurrentUser().getId())) {
            map.put("newFriends", service.getNotApprovedFriends(AuthUtil.getCurrentUser()));
            map.put("control", true);
        }
        User user = userService.getById(id);
        if(user == null) {
            map.put("message", "User not found");
            return "errors/template";
        }
        List<User> friends = service.getFriendsByUser(user);
        map.put("friends", friends);
        return "friends";
    }

    /*
     * @param id - id user to delete
     * @return id if friend delete, else return -1
     */
    @RequestMapping(value = "/delfr", method = RequestMethod.POST)
    public @ResponseBody String removeFriend(@RequestParam Long id) {
        System.out.println("DELETE FRIEND");
        if(service.remove(AuthUtil.getCurrentUser(), userService.getById(id))) {
            return String.valueOf(id);
        }
        return "-1";
    }

    @RequestMapping(value = "/addToFriend", method = RequestMethod.POST)
    public @ResponseBody String addFriend(@RequestParam Long id) {
        System.out.println("ADD FRIENDS");
        service.save(new Friend(AuthUtil.getCurrentUser(), new User(id)));
        return "ok";
    }

    @RequestMapping(value = "/proveFr", method = RequestMethod.POST)
    public @ResponseBody String proveFriend(@RequestParam Long id) {
        System.out.println(id);
        Friend f = service.getByUsers(new User(id), AuthUtil.getCurrentUser());
        System.out.println(f.getId());
        f.setApproved(true);
        service.approved(f);
        return "ok";
    }

}
