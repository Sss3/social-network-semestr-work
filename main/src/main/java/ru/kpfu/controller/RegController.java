package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.service.UserService;
import ru.kpfu.auth.PassEncoder;
import ru.kpfu.controller.validator.RegValidator;
import ru.kpfu.form.RegForm;
import ru.kpfu.mailing.MailSendService;
import ru.kpfu.util.AuthUtil;

import java.util.Random;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Controller
public class RegController {

    @Autowired
    private RegValidator validator;

    @Autowired
    private UserService userService;

    @Autowired
    private MailSendService mailSendService;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String renderPage(ModelMap map) {
        if(AuthUtil.isAuthenticated()) {
            return "redirect:/";
        }
        map.put("regform", new RegForm());
        return "util-page/reg-page";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String reg(@ModelAttribute("regform") RegForm form, BindingResult bindingResult, ModelMap map) {
        validator.validate(form, bindingResult);
        if(bindingResult.hasErrors()) {
            return "util-page/reg-page";
        }
        regUser(form);
        return "redirect:/login";
    }

    private void regUser(RegForm form) {
        User user = new User();
        user.setEmail(form.getEmail());
        String password = generatePassword(8);
        user.setPassword(new PassEncoder().encode(password));
        user.setName(form.getName());
        user.setFamily(form.getFamily());
        userService.save(user);
        mailSendService.sendInvite(form.getEmail(), password);
    }

    private String generatePassword(int length) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.!$%".toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++){
            char c = chars[random.nextInt(chars.length)];
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

}
