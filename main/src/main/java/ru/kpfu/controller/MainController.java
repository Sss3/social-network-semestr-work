package ru.kpfu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kpfu.util.AuthUtil;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
@Controller
public class MainController {

    @RequestMapping("/")
    public String renderMainPage() {
        if(AuthUtil.isAuthenticated()) {
            return "redirect:/id0";
        }
        return "util-page/helloPage";
    }

}
