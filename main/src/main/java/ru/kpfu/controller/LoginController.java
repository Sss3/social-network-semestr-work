package ru.kpfu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.util.AuthUtil;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String renderPage(@RequestParam(value = "error", required = false) String error, ModelMap map){
        if(AuthUtil.isAuthenticated()) {
            return "redirect:/";
        }
        if(error != null) {
            map.put("error", true);
        }
        return "util-page/login";
    }

}
