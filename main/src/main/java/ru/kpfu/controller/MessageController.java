package ru.kpfu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.corba.se.spi.ior.ObjectKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.api.domain.Message;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.service.MessageService;
import ru.kpfu.util.AuthUtil;
import ru.kpfu.util.MessageWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexeev Vladimir  06.04.2015
 */
@Controller
public class MessageController {

    @Autowired
    MessageService messageService;


    @Secured("ROLE_USER")
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public @ResponseBody String sendMessage(@RequestParam Long to, @RequestParam String text) throws JsonProcessingException {
        Message message = messageService.send(new Message(new User(to), AuthUtil.getCurrentUser(), text));
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new ru.kpfu.json.Message(message));
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/im", method = RequestMethod.GET)
    public String renderChat(ModelMap modelMap) {
        modelMap.put("messageList", getMessageWrapper(messageService.getForDialog(AuthUtil.getCurrentUser())));
        User u = AuthUtil.getCurrentUser();
        modelMap.put("currentName", u.getName() + " " + u.getFamily());
        String avatar = u.getAvatar() != null ? u.getAvatar() : "/resources/img/avatarDefault.png";
        modelMap.put("currentAvatar", avatar);
        return "chat";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/openD", method = RequestMethod.POST)
    public @ResponseBody String openDialog(@RequestParam Long id) throws JsonProcessingException {
        List<Message> messages = messageService.getForDialog(AuthUtil.getCurrentUser(), new User(id));
        ObjectMapper mapper = new ObjectMapper();
        List<ru.kpfu.json.Message> messageList = getJsonMessage(messages);
        return mapper.writeValueAsString(messageList);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/closeD", method = RequestMethod.POST)
    public @ResponseBody List<MessageWrapper> closeDialog() {
        System.out.println("hello bro :)");
        return getMessageWrapper(messageService.getForDialog(AuthUtil.getCurrentUser()));
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/getMessage", method = RequestMethod.POST)
    public @ResponseBody String getMessage(@RequestParam Long userId, @RequestParam Long messageId) throws JsonProcessingException {
        List<Message> messages = messageService.getNewMessage(AuthUtil.getCurrentUser(), new User(userId), messageId);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(getJsonMessage(messages));
    }

    public List<MessageWrapper> getMessageWrapper(List<Message> messages) {
        List<MessageWrapper> messageList = new ArrayList<>();
        for(Message m : messages) {
            messageList.add(new MessageWrapper(m));
        }
        return messageList;
    }

    public List<ru.kpfu.json.Message> getJsonMessage(List<Message> messages) {
        List<ru.kpfu.json.Message> messageList = new ArrayList<>();
        for(Message m : messages) {
            messageList.add(new ru.kpfu.json.Message(m));
        }
        return messageList;
    }

}
