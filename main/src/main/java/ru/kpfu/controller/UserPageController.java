package ru.kpfu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.api.domain.City;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.service.CityService;
import ru.kpfu.api.service.FriendService;
import ru.kpfu.api.service.UserService;
import ru.kpfu.api.service.WallPostService;
import ru.kpfu.auth.PassEncoder;
import ru.kpfu.controller.validator.ChangePassword;
import ru.kpfu.controller.validator.EditProfileValidator;
import ru.kpfu.form.EditProfileForm;
import ru.kpfu.form.PassForm;
import ru.kpfu.util.AuthUtil;

import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import java.io.*;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Controller
public class UserPageController {

    @Autowired
    private UserService userService;

    @Autowired
    private WallPostService wallPostService;

    @Autowired
    private CityService cityService;

    @Autowired
    private EditProfileValidator editProfileValidator;

    @Autowired
    private ChangePassword passValidator;

    @Autowired
    private FriendService friendService;

    @RequestMapping("/id{id}")
    public String renderPage(@PathVariable Long id, ModelMap map) {
        if(id.equals(0L)) {
            if(!AuthUtil.isAuthenticated()) return "redirect:/login";
            return "redirect:/id" + AuthUtil.getCurrentUser().getId();
        }
        User user = userService.getById(id);
        if(user == null) {
            map.put("message", "User not found");
            return "errors/template";
        }
        if(AuthUtil.isAuthenticated() && id.equals(AuthUtil.getCurrentUser().getId())) {
            map.put("edit", true);
            map.put("add", false);
        }
        if(AuthUtil.isAuthenticated() && !id.equals(AuthUtil.getCurrentUser().getId()) ) { //проверка списка друзей, если этого пользователя в нём нету, то true
            if(!friendService.isFriends(AuthUtil.getCurrentUser(), new User(id)))
            map.put("add", true);
        }
        map.put("friends", friendService.getListUserFriends(user, 3));
        map.put("wallPosts", wallPostService.getByTo(user));
        map.put("user", user);
        return "user-page";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String renderEditPage(ModelMap map) {
        User u = AuthUtil.getCurrentUser();
        EditProfileForm form = new EditProfileForm();
        form.setAbout(u.getAbout());
        form.setEmail(u.getEmail());
        form.setPhone(u.getPhone());
//        form.setCity(u.getCity().getId());
        map.put("editForm", form);
        map.put("cityList", cityService.getAll());
        map.put("passForm", new PassForm());
        return "util-page/edit-profile";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute("editForm") EditProfileForm form, BindingResult bindingResult, ModelMap map) {
//        editProfileValidator.validate(form, bindingResult);
//        if(bindingResult.hasErrors()) {
//            map.put("cityList", cityService.getAll());
//            return "edit-profile";
//        }
        update(form);
        return "redirect:/id0";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public String editPass(@ModelAttribute()PassForm form, BindingResult bindingResult, ModelMap map) {
        passValidator.validate(form, bindingResult);
        if(bindingResult.hasErrors()) {
            map.put("cityList", cityService.getAll());
            return "util-page/edit-profile";
        } else {
            if(!new PassEncoder().matches(form.getOld(), AuthUtil.getCurrentUser().getPassword())) {
                bindingResult.rejectValue("old", "pass.noEquals");
                map.put("cityList", cityService.getAll());
                return "util-page/edit-profile";
            }
        }

        User user = AuthUtil.getCurrentUser();
        user.setPassword(new PassEncoder().encode(form.getNewP()));
        userService.save(user);
        return "redirect:/id0";
    }


    private void update(EditProfileForm form) {
        User user = AuthUtil.getCurrentUser();
        if(form.getAvatar()!= null && !form.getAvatar().isEmpty()) {
            MultipartFile file = form.getAvatar();
            BufferedOutputStream stream = null;
            try {
                byte[] bytes = file.getBytes();
                File serverFile = save(user.getId(), file.getContentType());
                stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            user.setAvatar(Const.urlAvatar + user.getId() + "." + getContentType(file.getContentType()));
        }
        if(form.getSex() != null) user.setSex(form.getSex());
        if(form.getEmail() != null && !form.getEmail().isEmpty()) user.setEmail(form.getEmail());
        if(form.getCity() != null) user.setCity(new City(form.getCity()));
        if(form.getPhone() != null) user.setPhone(form.getPhone());
        if(form.getAbout() != null && !form.getAbout().isEmpty()) user.setAbout(form.getAbout());

        userService.save(user);
    }


    private File save(Long id, String contentType) {
        File f = new File(Const.uploadAvatar + File.separator + id + "." + getContentType(contentType));
        if(f.exists()) {
            f.delete();
        }
        return f;
    }

    private String getContentType(String contentType) {
        try {
            return new ContentType(contentType).getSubType();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


}
