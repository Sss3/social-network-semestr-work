package ru.kpfu.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.kpfu.api.domain.User;
import ru.kpfu.auth.AuthUser;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public class AuthUtil {

    private static boolean isNoAuth(Authentication authentication) {
        return authentication == null || !(authentication instanceof UsernamePasswordAuthenticationToken);
    }

    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !isNoAuth(authentication);
    }

    public static Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNoAuth(authentication) ? null : ((AuthUser) authentication.getPrincipal()).getUser().getId();
    }

    public static User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return isNoAuth(authentication) ? null : ((AuthUser) authentication.getPrincipal()).getUser();
    }

}
