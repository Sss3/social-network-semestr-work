package ru.kpfu.util;

import ru.kpfu.api.domain.Message;

import java.util.Date;

/**
 * @author Alexeev Vladimir  18.04.2015
 */
public class MessageWrapper {

    private Long id;
    private String avatar;
    private String name;
    private Date date;
    private String text;
    private Long messId;

    private MessageWrapper() {

    }

    public MessageWrapper(Message message) {
        if(message.getTo().getId().equals(AuthUtil.getCurrentUserId())) {
            this.id = message.getFrom().getId();
            this.avatar = message.getFrom().getAvatar() != null ? message.getFrom().getAvatar() : "/resources/img/avatarDefault.png";
            this.name = message.getFrom().getName() + " " + message.getFrom().getFamily();
        } else {
            this.id = message.getTo().getId();
            this.avatar = message.getTo().getAvatar() != null ? message.getTo().getAvatar() : "/resources/img/avatarDefault.png";
            this.name = message.getTo().getName() + " " + message.getTo().getFamily();
        }
        this.date = message.getDate();
        this.text = message.getText();
        this.messId = message.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getMessId() {
        return messId;
    }

    public void setMessId(Long messId) {
        this.messId = messId;
    }
}
