package ru.kpfu.json;

import ru.kpfu.util.AuthUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Alexeev Vladimir  06.04.2015
 */
public class Message implements Serializable{

    private static final long serialVersionUID = 7959722966400445908L;
    private Long to;
    private Long from;
    private Date date;
    private String text;
    private boolean me;
    private Long id;

    private Message() {

    }

    public Message(ru.kpfu.api.domain.Message message) {
        this.to = message.getTo().getId();
        this.from = message.getFrom().getId();
        this.date = message.getDate();
        this.text = message.getText();
        this.me = (AuthUtil.getCurrentUserId().equals(message.getTo().getId()))? true : false;
        this.id = message.getId();
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isMe() {
        return me;
    }

    public void setMe(boolean me) {
        this.me = me;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
