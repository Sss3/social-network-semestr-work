package ru.kpfu.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.kpfu.api.domain.WallPost;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public class Post {

    @JsonIgnore
    private WallPost post;

    private String name;
    private Long idFrom;
    private Long id;
    private String text;
    private String avatar;
    private Post() {
    }

    public Post(WallPost wallPost) {
        this.post = wallPost;

    }

    public void init() {
        this.name = post.getFrom().getName() + " " + post.getFrom().getFamily();
        this.idFrom = post.getFrom().getId();
        this.id = post.getId();
        this.text = post.getText();
        this.avatar = post.getFrom().getAvatar() != null ? post.getFrom().getAvatar() : "/resources/img/avatarDefault.png";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WallPost getPost() {
        return post;
    }

    public void setPost(WallPost post) {
        this.post = post;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
