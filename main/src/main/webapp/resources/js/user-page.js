var photoForPost = new Array();

function sendPost(id) {
    var message = $("#postmsg").val();
    if(message.length > 0) {

        $.post("/sendPost", {text: message, to: id}, addPost);
    }
}

function cleanTextArea() {
    textArea = $("#postmsg");
    textArea.val("");
}

function addPost(data) {
    var post = JSON.parse(data);

    $("#wall").prepend('<div class="media">'
    + '<div class="media-left">' +
    '<a href="/id' + post.idFrom + '"><img class="media-object img-circle" src="' + post.avatar + '" width="64" height="64"></a>' +
    '</div>' +
    '<div class="media-body">' +
    '<h4 class="media-heading"><a href="/id' + post.idFrom + '">' + post.name + '</a></h4>' +
    post.text +
    '</div></div>');

    cleanTextArea();
    photoForPost = new Array();
}



function attach() {

}

function attachWindowOpen() {

}

$(function() {
    var dialog, form
    message = $("#message");

    dialog = $("#dialog-message").dialog({
        autoOpen: false,
        modal: true,
        buttons: {

        }
    })
})