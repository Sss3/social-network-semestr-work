<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@tag description="Simple Template" pageEncoding="UTF-8" %>
<%@attribute name="title" %>
<%@attribute name="head" fragment="true" %>
<%@attribute name="body" fragment="true" required="true" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>${title}</title>
    <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
    <link href="/resources/css/canvas.css" rel="stylesheet">
    <script src="/resources/js/jquery.min.js"></script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>--%>
    <script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="/resources/js/jquery.min.js"></script>--%>
    <jsp:invoke fragment="head"/>
</head>
<body>

<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <security:authorize access="isAuthenticated()">
                    <li><a href="/im">Messages </a></li>
                    <li><a href="/friends">Friends</a></li>
                    <li><a href="/album">Photo</a></li>
                    <li><a href="/logout">Log Out</a></li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li><a href="/">Main</a></li>
                    <li><a href="/registration">Registration</a></li>
                    <li><a href="/login">Login</a></li>
                </security:authorize>

            </ul>
        </div>
        <!-- /.nav-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- /.navbar -->

<div class="container">

    <div class="row row-offcanvas row-offcanvas-left">

        <%--<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">--%>

        <%--<div class="list-group">--%>
        <%--<security:authorize access="isAuthenticated()">--%>
        <%--<a href="/" class="list-group-item">My page</a>--%>
        <%--<a href="/im" class="list-group-item">Messages </a>--%>
        <%--<a href="/friends" class="list-group-item">Friends</a>--%>
        <%--<a href="/album" class="list-group-item">Photo</a>--%>
        <%--</security:authorize>--%>
        <%--<security:authorize access="isAnonymous()">--%>
        <%--<a href="/" class="list-group-item">Main</a>--%>
        <%--<a href="/registration" class="list-group-item">Registration</a>--%>
        <%--<a href="/login" class="list-group-item">Login</a>--%>
        <%--</security:authorize>--%>
        <%--</div>--%>
        <%--</div><!--/.sidebar-offcanvas-->--%>

        <div class="col-xs-12 col-sm-12">
            <jsp:invoke fragment="body"/>
        </div>
        <!--/.col-xs-12.col-sm-9-->


    </div>
    <!--/row-->

    <hr/>

    <footer>
        <p>&copy; Julia Yakimova 2015</p>
    </footer>

</div>
<!--/.container-->
</body>
</html>