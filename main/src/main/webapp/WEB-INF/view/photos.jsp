<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template title="Photo album">
    <jsp:attribute name="head">
        <link rel="stylesheet" href="/resources/css/album.css">
        <link rel="stylesheet" href="/resources/css/uploader.css">
        <%--<script type="text/javascript" src="/resources/js/album.js"></script>--%>
        <script type="text/javascript" src="/resources/js/uploader.min.js"></script>
        <%--<script type="text/javascript" src="/resources/js/jquery.fancybox-1.2.1.pack.js"></script>--%>
    </jsp:attribute>

    <jsp:attribute name="body">
        <c:if test="${edit}">

            <div class="row">
                <div class="col-lg-12">
                    <div id="drag-and-drop-zone" class="uploader">
                        <div>Drag &amp; Drop Images Here</div>
                        <div class="or"></div>
                        <div class="browser">
                            <label>
                            <span class="btn btn-info btn-lg btn-block"><span
                                    class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Add photo</span>
                                <input type="file" name="file" multiple="multiple" title='Click to add Files'>
                            </label>
                        </div>
                    </div>
                </div>

            </div>

        </c:if>
        <br/>
        <div class="wrap" id="wrap">
            <c:if test="${not empty photos}">
                <c:forEach items="${photos}" var="photo">
                    <div class="box">
                        <div class="boxInner">
                            <a <%--href="#"--%> onclick="openPhoto('${photo.url}')"><img src="${photo.url}"/></a>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true" id="photoModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" id="photoView">

                </div>
            </div>
        </div>

    <script type="text/javascript">


        $("#drag-and-drop-zone").dmUploader({
            url: '/upload/photo',
            dataType: "json",
            onUploadSuccess: function (id, data) {

                $("#wrap").append('<div class="box">' +
                '<div class="boxInner">' +
                '<a onclick="openPhoto("' + data.url + '")">' +
                '<img src="' + data.url + '"/></a>' +
                '</div></div>');
            }
        });

        function openPhoto(url) {
            $("#photoView").html("");
            $("#photoView").append('<img src="' + url + '" class="img-responsive center-block"/>');
            $("#photoModal").modal('show');
        }

    </script>
    </jsp:attribute>
</t:template>