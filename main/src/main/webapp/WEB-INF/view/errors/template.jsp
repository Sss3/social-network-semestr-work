<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Error">

    <jsp:attribute name="body">
        <div class="alert alert-danger" role="alert">${message}</div>
    </jsp:attribute>

</t:template>