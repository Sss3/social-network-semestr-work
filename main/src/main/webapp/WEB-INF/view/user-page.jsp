<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<t:template title="${user.name} &#32; ${user.family}">
    <jsp:attribute name="head">
        <link rel="stylesheet" href="/resources/css/my.css">

        <%--<script src="/resources/js/user-page.js"></script>--%>
    </jsp:attribute>

    <jsp:attribute name="body">
        <div class="row">
		  <div class="col-lg-4">
              <c:if test="${not empty user.avatar}">
                  <img src="${user.avatar}" class="img-responsive img-rounded center-block"/>
                  <br/>
              </c:if>
              <c:if test="${empty user.avatar}">
                  <img src="/resources/img/avatarDefault.png" class="img-responsive img-rounded center-block"/>
              </c:if>
              <br/>
              <sec:authorize access="isAuthenticated()">
                  <c:if test="${not edit}">
                  <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#messageModal">
                      Send message
                  </button>
                      <br/>
                  </c:if>
                  <c:if test="${edit}">
                      <a href="/edit" class="btn btn-info btn-lg btn-block">Edit profile</a><br/>
                  </c:if>
                  <c:if test="${add}">
                      <div id="addFr">
                      <button  class="btn btn-info btn-lg btn-block" onclick="addToFriends('${user.id}')">Add to friends</button>
                      </div>
                  </c:if>
                  <br/>
              </sec:authorize>

              <c:if test="${not empty friends}">
              <div class="panel panel-info">
                  <div class="panel-heading">
                      <h3 class="panel-title">Friends</h3>
                  </div>
                  <div class="panel-body">
                      <div class="row">

                          <c:forEach items="${friends}" var="friend">
                              <div class="col-lg-4">
                              <a href="/id${friend.id}">
                                  <c:if test="${not empty friend.avatar}">
                                      <img class="media-object img-circle center-block" src="${friend.avatar}" width="50"
                                           height="50">
                                  </c:if>
                                  <c:if test="${empty friend.avatar}">
                                      <img class="media-object img-circle center-block" src="/resources/img/avatarDefault.png"
                                           width="50" height="50">
                                  </c:if>
                              </a>
                                  <div class="text-center"> <a href="/id${friend.id}">${friend.name}</a> </div>
                              </div>
                          </c:forEach>
                          </div>
                  </div>
              </div>
              </c:if>

          </div>
		  <div class="col-lg-8">
              <h2>${user.name} &#32; ${user.family}</h2>
              <hr/>
              <c:if test="${not empty user.sex}">
                  <p><strong>Sex :</strong> ${user.sex}</p>
              </c:if>
              <c:if test="${not empty user.city}">
                  <p><strong>City :</strong> ${user.city.name}</p>
              </c:if>
              <c:if test="${not empty user.phone}">
                  <p><strong>Phone :</strong> ${user.phone}</p>
              </c:if>
              <c:if test="${not empty user.about}">
                  <p><strong>About me :</strong> ${user.about}</p>
              </c:if>

              <hr/>
              <div class="panel panel-info">
                  <div class="panel-heading"><h3 class="panel-title">Wall</h3></div>
                  <br/>
                  <sec:authorize access="isAuthenticated()">
                      <div class="send-post">

                          <form id="wallpost">
                              <textarea class="form-control text-a" form="wallpost" rows="3" id="postmsg"></textarea>
                              <br/>
                              <button type="button" class="btn btn-primary" onclick="sendPost(${user.id})">Share
                              </button>
                          </form>
                              <%--<button type="button" class="btn btn-primary" data-toggle="modal"--%>
                              <%--data-target="#addPhotoModal">Attach photo</button>--%>
                      </div>
                      <hr/>
                  </sec:authorize>

                  <div id="wall">
                      <c:forEach items="${wallPosts}" var="wpost">
                          <div class="media">
                              <div class="media-left send-post">
                                  <a href="/id${wpost.from.id}">
                                      <c:if test="${not empty wpost.from.avatar}">
                                          <img class="media-object img-circle" src="${wpost.from.avatar}" width="64"
                                               height="64">
                                      </c:if>
                                      <c:if test="${empty wpost.from.avatar}">
                                          <img class="media-object img-circle" src="/resources/img/avatarDefault.png"
                                               width="64" height="64">
                                      </c:if>
                                  </a>
                              </div>
                              <div class="media-body">
                                  <h4 class="media-heading"><a
                                          href="/id${wpost.from.id}">${wpost.from.name} ${wpost.from.family}</a></h4>
                                      <c:out value="${wpost.text}"/>
                                  <br/>
                                  <h5>
                                      <small><fmt:formatDate value="${wpost.date}" type="date"/> <fmt:formatDate
                                              value="${wpost.date}" type="time" timeStyle="short"/></small>
                                  </h5>
                              </div>
                          </div>
                      </c:forEach>
                  </div>
              </div>
          </div>


        <!-- Modal -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send message to ${user.name}</h4>
                    </div>
                    <div class="modal-body">
                            <textarea class="form-control text-a" rows="6" id="msg"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="sendMessage(${user.id})">Send</button>
                    </div>
                </div>
            </div>
        </div>

       <script type="text/javascript">

           function sendPost(id) {
               var message = $("#postmsg").val();
               if(message.length > 0) {

                   $.post("/sendPost", {text: message, to: id}, addPost);
               }
           }

           function addToFriends(id) {
               $.post("/addToFriend", {id: id}, dissableButtonFr);
           }

           function dissableButtonFr() {
               $("#addFr").html("");
           }

            function sendMessage(id) {
                var message = $("#msg").val();
                if(message.length > 0) {
                    $.post("/sendMessage", {text: message, to:id}, closeModal);
                }
            }

           function closeModal() {
               $("#messageModal").modal('hide');
           }

           function cleanTextArea() {
               textArea = $("#postmsg");
               textArea.val("");
           }

           function addPost(data) {
               var post = JSON.parse(data);

               $("#wall").prepend('<div class="media">'
               + '<div class="media-left">' +
               '<a href="/id' + post.idFrom + '"><img class="media-object img-circle" src="' + post.avatar + '" width="64" height="64"></a>' +
               '</div>' +
               '<div class="media-body">' +
               '<h4 class="media-heading"><a href="/id' + post.idFrom + '">' + post.name + '</a></h4>' +
               post.text +
               '</div></div>');

               cleanTextArea();
           }

        </script>

    </jsp:attribute>

</t:template>