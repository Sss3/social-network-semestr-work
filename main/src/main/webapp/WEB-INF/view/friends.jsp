<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<t:template title="Friends">
    <jsp:attribute name="head">

        <link rel="stylesheet" href="/resources/css/my.css">
    </jsp:attribute>
    <jsp:attribute name="body">
        <c:if test="${not empty newFriends}">
            <h3>New friends</h3>
            <c:forEach items="${newFriends}" var="newFriend">
                <div class="row" id="friend${newFriend.id}">
                <div class="col-lg-3">
                    <a href="/id${newFriend.id}">
                        <c:if test="${not empty newFriend.avatar}">
                            <img class="media-object img-circle" src="${newFriend.avatar}" width="128"
                                 height="128">
                        </c:if>
                        <c:if test="${empty newFriend.avatar}">
                            <img class="media-object img-circle" src="/resources/img/avatarDefault.png"
                                 width="128" height="128">
                        </c:if>
                    </a>
                </div>
                <div class="col-lg-5">
                    <a href="/id${newFriend.id}"><h3>${newFriend.family} ${newFriend.name}</h3></a>
                </div>
                <div class="col-lg-4">
                    <ul class="ListNone">
                        <li></li>
                        <li><a onclick="approveFr('${newFriend.id}')">Add</a></li>
                        <li></li>
                        <li><a onclick="delFriend('${newFriend.id}')">Reject</a> </li>
                    </ul>
                </div>
            </c:forEach>
            <hr/>
        </c:if>

    <c:forEach items="${friends}" var="friend">
        <div class="row" id="friend${friend.id}">
            <div class="col-lg-3">
                <a href="/id${friend.id}">
                <c:if test="${not empty friend.avatar}">
                    <img class="media-object img-circle" src="${friend.avatar}" width="128"
                         height="128">
                </c:if>
                <c:if test="${empty friend.avatar}">
                    <img class="media-object img-circle" src="/resources/img/avatarDefault.png"
                         width="128" height="128">
                </c:if>
                </a>
            </div>
            <div class="col-lg-5">
                <a href="/id${friend.id}"><h3>${friend.family} ${friend.name}</h3></a>
            </div>
            <c:if test="${control}">
                <div class="col-lg-4">
                    <ul class="ListNone">
                        <li></li>
                        <li><a onclick="delFriend('${friend.id}')">Delete</a></li>
                        <li></li>
                        <li><a href="/im?id=${friend.id}">Send message</a></li>
                    </ul>
                </div>
            </c:if>

        </div>
        <br/>
    </c:forEach>

        <script type="text/javascript">
            function delFriend(id) {
                $.post("/delfr", {to: id}, del);
            }

            function del(id) {
                fr = $("#friend" + id);
                fr.html("");
            }

            function approveFr(id) {
                alert("PROVE");
                $.post("/proveFr", {id: id}, del);
            }

        </script>


    </jsp:attribute>
</t:template>