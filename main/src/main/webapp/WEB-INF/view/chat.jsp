<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<t:template title="Dialogues">
    <jsp:attribute name="head">
        <link rel="stylesheet" type="text/css" href="/resources/css/chat.css">
        <script type="text/javascript" src="/resources/js/chat.js"></script>
    </jsp:attribute>
    <jsp:attribute name="body">
        <h3 id="header">Count dialogues : ${fn:length(messageList)}</h3>
        <hr/>
            <div id="device">
                <div class="chat" id="chat">
                    <c:forEach items="${messageList}" var="mess">
                        <div class="message" onclick="openD(${mess.id}, ${mess.messId})">
                            <img src="${mess.avatar}" id="av${mess.id}">
                            <div><h5 id="n${mess.id}">${mess.name}</h5><p>${mess.text} <br/> ${mess.date} </p></div>
                        </div>
                    </c:forEach>
                </div>
            </div>

        <script type="text/javascript">
            var currentId = 15;
            var nameMe = "${currentName}";
            var nameInterlocutor;
            var photoMe = "${currentAvatar}";
            var photoInterlocutor;
            var lastMessage;
            var bysy = false;
            var timer;

            function sendMessage() {
                var message = $("#messageText").val();
                if (message.length > 0) {
                    $.post("/sendMessage", {text: message, to: currentId}, addMess);
                }
            }

            function addMess(data) {
                $("#messageText").val("");
                var b = JSON.parse(data);
                var messageDate = new Date(b.date);
                $("#chat").append('<div class="message">' +
                '<img src="' + photoMe + '" />' +
                '<div><p><strong>' + nameMe + ' : </strong>' + b.text + '<br/>' + messageDate.toLocaleDateString("ru", {
                    hour : "numeric",
                    minute : "numeric"
                }) + ' </p></div>' +
                '</div>');
            }

            function openD(id, last) {
                currentId = id;
                lastMessage = last;
                nameInterlocutor = $("#n" + id).html();
                photoInterlocutor = $("#av" + id).attr("src");
                $.post("/openD", {id: id}, dialog);
                timer = setInterval(function() {
                    if(!bysy) {
                        getMessage();
                    }
                }, 3000);
            }

            function dialog(data) {
                var a = JSON.parse(data);
                $("#header").html("Back");
                $("#header").on('click', function() {
                    closeDialog();
                });
                $("#chat").html("");
                $.each(a, function () {
                    var messageDate = new Date(this.date);
                    if (this.me) {
                        $("#chat").append('<div class="message me">' +
                        '<img src="' + photoInterlocutor + '" />' +
                        '<div><p><strong>' + nameInterlocutor + ' : </strong>' + this.text + '<br/>' + messageDate.toLocaleDateString("ru", {
                            hour : "numeric",
                            minute : "numeric"
                        }) + '</p></div>' +
                        '</div>');
                    } else {
                        $("#chat").append('<div class="message">' +
                        '<img src="' + photoMe + '" />' +
                        '<div><p><strong>' + nameMe + ' : </strong>' + this.text + '<br/>' + messageDate.toLocaleDateString("ru", {
                            hour : "numeric",
                            minute : "numeric"
                        }) + ' </p></div>' +
                        '</div>');
                    }
                });
                $("#device").append('<br/><textarea class="form-control" id="messageText"></textarea>'
                                    + '<br/> <button type="button" class="btn btn-primary btn-sm" onclick="sendMessage()">Send</button>');

            }

            function getMessage() {
                $.post("/getMessage", {userId: currentId, messageId: lastMessage}, displayMessage);
            }

            function displayMessage(data) {
                bysy = true;
                var a = JSON.parse(data);
                if(data.length > 0) {
                    var chat = $("#chat");
                    $.each(a, function() {
                        var messageDate = new Date(this.date);
                        $("#chat").append('<div class="message me">' +
                        '<img src="' + photoInterlocutor + '" />' +
                        '<div><p><strong>' + nameInterlocutor + ' : </strong>' + this.text + '<br/>' + messageDate.toLocaleDateString("ru", {
                            hour : "numeric",
                            minute : "numeric"
                        }) + '</p></div>' +
                        '</div>');
                        lastMessage = this.id;
                    });
                };
                bysy = false;
            }

            function closeDialog() {
                clearInterval(timer);

                $.post("/closeD", renderPage);
            }
            function renderPage(date) {
                $("#header").html("Count dialogues : ");
                $("#device").html("");
                $("#device").html("<div class='chat' id='chat'></div>");
                $.each(date, function() {
                    var messageDate = new Date(this.date);
                    $("#chat").append('<div class="message"  onclick="openD(' + this.id  + ',' + this.messId + ')">' +
                    '<img src="'+ this.avatar + '" id="av' + this.id + '">' +
                    '<div><h5 id="n' + this.id + '">' + this.name + '</h5><p>' + this.text + '<br/>' +  messageDate.toLocaleDateString("ru", {
                        hour : "numeric",
                        minute : "numeric"
                    }) + '</p></div>' +
                    '</div>');
                });
            }
        </script>
    </jsp:attribute>

</t:template>
