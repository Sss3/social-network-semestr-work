<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<t:template title="Edit profile">
    <jsp:attribute name="body">

        <div class="row">

            <div class="col-lg-12">
                <div class="alert alert-info" role="alert">
                    Fill in the fields required to change.
                </div>

                <form:form action="/edit" modelAttribute="editForm" cssClass="form-horizontal" method="post"
                           enctype="multipart/form-data">
                    <div class="form-group">
                        <form:label path="email" cssClass="col-sm-2 control-label">Email</form:label>
                        <div class="col-sm-10">
                            <form:input path="email" cssClass="form-control" placeholder="Email"/>
                            <form:errors path="email" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="sex" cssClass="col-sm-2 control-label">Sex</form:label>
                        <div class="col-sm-10">
                            <form:select path="sex" cssClass="form-control">
                                <form:option value="Male">Male</form:option>
                                <form:option value="Female">Female</form:option>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="city" cssClass="col-sm-2 control-label">City</form:label>
                        <div class="col-sm-10">
                            <form:select path="city" cssClass="form-control">
                                <c:forEach items="${cityList}" var="city">
                                    <form:option value="${city.id}">${city.name}</form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="phone" cssClass="col-sm-2 control-label">Phone</form:label>
                        <div class="col-sm-10">
                            <form:input path="phone" type="number" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="about" cssClass="col-sm-2 control-label">About me</form:label>
                        <div class="col-sm-10">
                            <form:textarea path="about" cssClass="form-control" rows="4"></form:textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="avatar" cssClass="col-sm-2 control-label">Photo</form:label>
                        <div class="col-sm-10">
                            <form:input path="avatar" type="file" cssClass="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Edit</button>
                        </div>
                    </div>
                </form:form>

                <hr/>

                <form:form cssClass="form-horizontal" method="post" modelAttribute="passForm">
                    <div class="form-group">
                        <form:label path="old" cssClass="col-sm-2 control-label">Old password</form:label>
                        <div class="col-sm-10">
                            <form:input path="old" type="password" cssClass="form-control"/>
                            <form:errors path="old" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="newP" cssClass="col-sm-2 control-label">New password (length > 6)</form:label>
                        <div class="col-sm-10">
                            <form:input path="newP" type="password" cssClass="form-control"/>
                            <form:errors path="newP" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="conf" cssClass="col-sm-2 control-label">Confirm password</form:label>
                        <div class="col-sm-10">
                            <form:input path="conf" type="password" cssClass="form-control"/>
                            <form:errors path="conf" cssClass="label label-danger"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Save password</button>
                        </div>
                    </div>
                </form:form>
            </div>

        </div>


    </jsp:attribute>
</t:template>