<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<t:template title="Main Page">
    <jsp:attribute name="body">
<p class="pull-right visible-xs">
    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
</p>
<div class="jumbotron">
    <h1>kpfu.ru</h1>

    <p>The Social Network for the semester work. Julia Yakimova</p>
</div>
<div class="row">
    <div class="col-xs-6 col-lg-4">
            <div class="thumbnail">
                <img src="" alt="...">
                <div class="caption">
                    <h3>Profile page</h3>
                    <p>Access to user page can be obtained without authorization.</p>
                </div>
            </div>
    </div>
    <!--/.col-xs-6.col-lg-4-->
    <div class="col-xs-6 col-lg-4">
        <div class="thumbnail">
            <img src="" alt="...">
            <div class="caption">
                <h3>Messages page</h3>
                <p>Write a message can only authorized users</p>
            </div>
        </div>
    </div>
    <!--/.col-xs-6.col-lg-4-->
    <div class="col-xs-6 col-lg-4">
        <div class="thumbnail">
            <img src="" alt="...">
            <div class="caption">
                <h3>Friends page</h3>
            </div>
        </div>
    </div>
    <!--/.col-xs-6.col-lg-4-->
    <div class="col-xs-6 col-lg-4">
        <div class="thumbnail">
            <img src="" alt="...">
            <div class="caption">
                <h3>Photos page</h3>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-lg-4">
        <div class="thumbnail">
            <img src="" alt="...">
            <div class="caption">
                <h3>Edit profile page</h3>
            </div>
        </div>
    </div>
    <!--/.col-xs-6.col-lg-4-->
</div><!--/row-->
    </jsp:attribute>
</t:template>

