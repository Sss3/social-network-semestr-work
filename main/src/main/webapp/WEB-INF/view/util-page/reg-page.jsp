<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:template title="Registration Page">
    <jsp:attribute name="body">

        <div class="row">

            <div class="col-lg-12">
                <div class="alert alert-info" role="alert">
                    All fields are required.
                </div>
                <form:form action="/registration" modelAttribute="regform" cssClass="form-horizontal" method="post">
                    <div class="form-group">
                        <form:label path="email" cssClass="col-sm-2 control-label">Email</form:label>
                        <div class="col-sm-10">
                            <form:input path="email" cssClass="form-control" placeholder="Email"/>
                            <form:errors path="email" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="name" cssClass="col-sm-2 control-label">Name</form:label>
                        <div class="col-sm-10">
                            <form:input path="name" cssClass="form-control" placeholder="Name"/>
                            <form:errors path="name" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <form:label path="family" cssClass="col-sm-2 control-label">Family</form:label>
                        <div class="col-sm-10">
                            <form:input path="family" cssClass="form-control" placeholder="Family"/>
                            <form:errors path="family" cssClass="label label-danger"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit"  class="btn btn-success">Registration</button>
                        </div>
                    </div>
                </form:form>
            </div>

        </div>

    </jsp:attribute>
</t:template>
