package ru.kpfu.api.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
@Entity
public class Friend implements Serializable{

    private static final long serialVersionUID = 2863870798160820440L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_1")
    private User user1;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_2")
    private User user2;

    private Boolean approved;

    public Friend() {
    }

    public Friend(User user1, User user2) {
        this.user1 = user1;
        this.user2 = user2;
        approved = false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
