package ru.kpfu.api.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Entity
@Table(name = "city")
public class City implements Serializable{

    private static final long serialVersionUID = -9101234202376177682L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    public City() {
    }

    public City(Long id) {
        this.id = id;
    }

    public City(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
