package ru.kpfu.api.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Alexeev Vladimir  31.03.2015
 */
@Entity
public class Photo implements Serializable{

    private static final long serialVersionUID = -3486971913748482978L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String url;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    public Photo() {

    }

    public Photo(Long id) {
        this.id = id;
    }

    public Photo(String url) {
        this.url = url;
        this.date = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
