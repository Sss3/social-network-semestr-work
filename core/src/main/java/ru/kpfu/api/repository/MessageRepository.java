package ru.kpfu.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.api.domain.Message;
import ru.kpfu.api.domain.User;

import java.util.List;

/**
 * @author Alexeev Vladimir  06.04.2015
 */
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByToOrFrom(User to, User from, Pageable pageable);

    List<Message> findByToOrFromOrderByDateAsc(User to, User from);

    @Query("select m from Message as m where (m.to=:u1 and m.from=:u2) or (m.to=:u2 and m.from=:u1) order by m.date asc")
    List<Message> findByTwoUsers(@Param("u1") User u1, @Param("u2") User u2);

    @Query("select m from Message as m where m.to=:user or m.from=:user")
    List<Message> findByUser(@Param("user") User user);

    @Query("select m from Message as m where m.to=:to and m.from=:from and m.id >:last order by m.date asc")
    List<Message> findNewMessage(@Param("to") User to, @Param("from") User from, @Param("last") Long lastMessage);
}
