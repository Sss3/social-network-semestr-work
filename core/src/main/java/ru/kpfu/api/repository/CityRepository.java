package ru.kpfu.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.api.domain.City;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public interface CityRepository extends CrudRepository<City, Long> {
}
