package ru.kpfu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.domain.WallPost;

import java.util.List;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public interface WallPostRepository extends JpaRepository<WallPost, Long> {

    public List<WallPost> findByToOrderByDateDesc(User to);

}
