package ru.kpfu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.api.domain.User;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
public interface UserRepository extends JpaRepository<User, Long> {

    public User findByEmail(String email);
}
