package ru.kpfu.api.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.api.domain.Photo;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
public interface PhotoRepository extends CrudRepository<Photo, Long> {



}
