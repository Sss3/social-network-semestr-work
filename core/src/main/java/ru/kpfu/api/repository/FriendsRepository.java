package ru.kpfu.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.api.domain.Friend;
import ru.kpfu.api.domain.User;

import java.util.List;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
public interface FriendsRepository extends JpaRepository<Friend, Long> {

    @Query(value = "select f from Friend as f where (f.user1=:user or f.user2=:user) and f.approved=true")
    public List<Friend> findByUser(@Param("user") User user);

    @Query("select f from Friend as f where (f.user1=:user1 and f.user2=:user2) or (f.user1=:user2 and f.user2=:user1)")
    public Friend findByUser1AndUser2(@Param("user1")User user1, @Param("user2")User user2);

    @Query("select f.user1 from Friend as f where f.user2=:user and f.approved=false")
    public List<User> findNotApprovedFriend(@Param("user")User user);
}
