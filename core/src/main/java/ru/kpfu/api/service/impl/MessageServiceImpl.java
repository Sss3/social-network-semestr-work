package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.Message;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.repository.MessageRepository;
import ru.kpfu.api.service.MessageService;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Alexeev Vladimir  06.04.2015
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageRepository messageRepository;

    @Override
    public Message send(Message message) {
        return messageRepository.saveAndFlush(message);
    }

    @Override
    public void remove(Message message) {
        messageRepository.delete(message);
    }

    @Override
    public List<Message> getLastMessage(User to, User from, int count) {
        return messageRepository.findByToOrFrom(to, from, new PageRequest(1, count, new Sort(Sort.Direction.ASC, "date")));
    }

    @Override
    public List<Message> getAllMessage(User user1, User user2) {
        return messageRepository.findByToOrFromOrderByDateAsc(user1, user2);
    }

    @Override
    public List<Message> getForDialog(User user) {
        List<Message> messages = new ArrayList<>();
        List<Message> allMessage = messageRepository.findByUser(user);
        for(Message m : allMessage) {
            add(messages, m);
        }
        return messages;
    }

    @Override
    public List<Message> getForDialog(User user1, User user2) {
        return messageRepository.findByTwoUsers(user1, user2);
    }

    @Override
    public List<Message> getNewMessage(User to, User from, Long lastMessage) {
        return messageRepository.findNewMessage(to, from, lastMessage);
    }

    public void add(List<Message> messages, Message message) {
        for(Message m : messages) {
            if((m.getFrom().getId().equals(message.getFrom().getId()) && m.getTo().getId().equals(message.getTo().getId())) ||
                    (m.getFrom().getId().equals(message.getTo().getId()) && m.getTo().getId().equals(message.getFrom().getId()))) {
                messages.remove(m);
                messages.add(message);
                return;
            }
        }
        messages.add(message);
    }
}
