package ru.kpfu.api.service;

import ru.kpfu.api.domain.Message;
import ru.kpfu.api.domain.User;

import java.util.List;
import java.util.Set;

/**
 * @author Alexeev Vladimir  06.04.2015
 */
public interface MessageService {

    public Message send(Message message);

    public void remove(Message message);

    public List<Message> getLastMessage(User to, User from, int count);

    public List<Message> getAllMessage(User user1, User user2);

    public List<Message> getForDialog(User user);

    public List<Message> getForDialog(User user1, User user2);

    public List<Message> getNewMessage(User to, User from, Long lastMessage);

}
