package ru.kpfu.api.service;

import ru.kpfu.api.domain.Friend;
import ru.kpfu.api.domain.User;

import java.util.List;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
public interface FriendService {

    public List<User> getFriendsByUser(User user);

    public Friend save(Friend friend);

    public boolean remove(User u1, User u2);

    public List<User> getListUserFriends(User user, int size);

    public List<User> getNotApprovedFriends(User user);

    public boolean isFriends(User u1, User u2);

    public Friend getByUsers(User u1, User u2);

    public Friend approved(Friend friend);

}
