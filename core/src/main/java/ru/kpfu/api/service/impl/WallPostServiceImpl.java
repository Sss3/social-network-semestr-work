package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.domain.WallPost;
import ru.kpfu.api.repository.WallPostRepository;
import ru.kpfu.api.service.WallPostService;

import java.util.List;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Service
public class WallPostServiceImpl implements WallPostService {

    @Autowired
    private WallPostRepository wallPostRepository;

    @Override
    public List<WallPost> getByTo(User to) {
        return wallPostRepository.findByToOrderByDateDesc(to);
    }

    @Override
    public WallPost save(WallPost wallPost) {
        return wallPostRepository.saveAndFlush(wallPost);
    }
}
