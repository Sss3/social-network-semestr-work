package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.Friend;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.repository.FriendsRepository;
import ru.kpfu.api.service.FriendService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
@Service
public class FriendServiceImpl  implements FriendService {

    @Autowired
    FriendsRepository repository;

    @Override
    public List<User> getFriendsByUser(User user) {
        return getUserList(repository.findByUser(user), user);
    }

    @Override
    public Friend save(Friend friend) {
        Friend f = repository.findByUser1AndUser2(friend.getUser1(), friend.getUser2());
        if(f != null) return f;
        return repository.saveAndFlush(friend);
    }

    public Friend approved(Friend friend) {
        return repository.saveAndFlush(friend);
    }

    @Override
    public boolean remove(User u1, User u2) {
        Friend friend = repository.findByUser1AndUser2(u1, u2);
        if(friend == null) repository.findByUser1AndUser2(u2, u1);
        if(friend == null) return false;
        repository.delete(friend);
        return true;
    }

    @Override
    public List<User> getListUserFriends(User user, int size) {
        List<User> ret = new ArrayList<>();
        List<User> tmp = getUserList(repository.findByUser(user), user);
        Random rd = new Random();
        if(tmp.size() <= size) return tmp;
        for(int i = 0; i < size; i++) {
            int t = rd.nextInt(tmp.size());
            ret.add(tmp.get(t));
            tmp.remove(t);
        }
        return ret;
    }

    @Override
    public List<User> getNotApprovedFriends(User user) {
        return repository.findNotApprovedFriend(user);
    }

    @Override
    public boolean isFriends(User u1, User u2) {
        if(repository.findByUser1AndUser2(u1, u2) != null) return true;
        return false;
    }

    @Override
    public Friend getByUsers(User u1, User u2) {
        return repository.findByUser1AndUser2(u1, u2);
    }

    private List<User> getUserList(List<Friend> friends, User user) {
        List<User> users = new ArrayList<>();
        for(Friend friend : friends) {
            if(!friend.getUser1().getId().equals(user.getId())) {
                users.add(friend.getUser1());
            } else {
                users.add(friend.getUser2());
            }
        }
        return users;
    }
}
