package ru.kpfu.api.service;

import ru.kpfu.api.domain.City;

import java.util.List;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public interface CityService {

    public List<City> getAll();

    public City save(City city);

    public City getById(Long id);


}
