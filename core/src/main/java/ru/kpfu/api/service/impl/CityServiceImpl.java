package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.City;
import ru.kpfu.api.repository.CityRepository;
import ru.kpfu.api.service.CityService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;

    @Override
    public List<City> getAll() {
        return (ArrayList<City>) cityRepository.findAll();
    }

    @Override
    public City save(City city) {
        return cityRepository.save(city);
    }

    @Override
    public City getById(Long id) {
        return cityRepository.findOne(id);
    }
}
