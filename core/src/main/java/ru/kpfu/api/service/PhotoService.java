package ru.kpfu.api.service;

import ru.kpfu.api.domain.Photo;
import ru.kpfu.api.domain.User;

import java.util.List;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
public interface PhotoService {

    public Photo save(Photo photo);

    public Photo getById(Long id);

    public void remove(Photo photo);

}
