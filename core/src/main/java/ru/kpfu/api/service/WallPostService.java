package ru.kpfu.api.service;

import ru.kpfu.api.domain.User;
import ru.kpfu.api.domain.WallPost;

import java.util.List;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public interface WallPostService {

    public List<WallPost> getByTo(User to);

    public WallPost save(WallPost wallPost);
}
