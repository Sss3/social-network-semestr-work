package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.Photo;
import ru.kpfu.api.repository.PhotoRepository;
import ru.kpfu.api.service.PhotoService;

/**
 * @author Alexeev Vladimir  02.04.2015
 */
@Service
public class PhotoServiceImpl implements PhotoService{

    @Autowired
    PhotoRepository photoRepository;

    @Override
    public Photo save(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public Photo getById(Long id) {
        return photoRepository.findOne(id);
    }

    @Override
    public void remove(Photo photo) {
        photoRepository.delete(photo);
    }
}
