package ru.kpfu.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.api.domain.User;
import ru.kpfu.api.repository.UserRepository;
import ru.kpfu.api.service.UserService;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User save(User user) {
        return userRepository.saveAndFlush(user);
    }

    public User getById(Long id) {
        return userRepository.findOne(id);
    }
}
