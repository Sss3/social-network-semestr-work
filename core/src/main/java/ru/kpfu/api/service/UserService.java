package ru.kpfu.api.service;

import ru.kpfu.api.domain.User;

/**
 * @author Alexeev Vladimir  29.03.2015
 */
public interface UserService {

    public User getByEmail(String email);

    public User save(User user);

    public User getById(Long id);

}
