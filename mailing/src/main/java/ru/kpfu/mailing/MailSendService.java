package ru.kpfu.mailing;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
public interface MailSendService {

    public void sendInvite(String to, String pass);

}
