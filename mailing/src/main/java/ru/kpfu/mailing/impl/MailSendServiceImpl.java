package ru.kpfu.mailing.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import ru.kpfu.mailing.MailSendService;

/**
 * @author Alexeev Vladimir  30.03.2015
 */
@Service
public class MailSendServiceImpl implements MailSendService{

    @Autowired
    MailSender mailSender;

    @Override
    public void sendInvite(String to, String pass) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject("Registration on Social Network");
        message.setText("Password " + pass);
        mailSender.send(message);
    }
}
